package main

import (
	"fmt"
	"io/ioutil"
	"regexp"

	sv "github.com/Masterminds/semver/v3"
)

// Using backticks for makeing sure that it's escaped properly
// This searches in all the texts, not beginning, nor end
const svregex = `(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?`

func main() {
	files := []string{"example.txt", "example2.txt"}

	// Loop through files and then check for valid SemVers.
	// Files might contain more than one occurences.
	filesData := []*sv.Version{}

	for _, file := range files {
		read, err := ioutil.ReadFile(file)
		if err != nil {
			panic(err)
		}

		re := regexp.MustCompile(svregex)
		versions := re.FindAllString(string(read), -1)

		if len(versions) != 0 {
			for _, version := range versions {
				nv, err := sv.NewVersion(version)

				if err != nil {
					fmt.Printf("Could not convert version string: %v\n", err)
					continue
				}

				filesData = append(filesData, nv)
			}
		}

	}

	for _, version := range filesData {
		fmt.Println(version.Major(), version.Minor(), version.Patch())
	}

	// Replace in each file

	str := "1.0.0-beta.0.93+sha512.498123-45813405938-04958-3902459-0385"

	v, err := sv.NewVersion(str)
	if err != nil {
		fmt.Println("Got the error: ", err)
	}

	fmt.Printf("Major: %d, minor: %d, patch: %d, meta: %s\n", v.Major(), v.Minor(), v.Patch(), v.Metadata())
}
