.PHONY: run clean all examples

run:
	go run .

clean:
	-rm example.txt example2.txt

examples: clean
	@echo "10.2.3-alpha.0.30+skjflsakdf-09324023949fsda90fs" > example.txt
	@echo "1.2.3-alpha.0.30+skjflsakdf-09324023949fsda90fs" >> example.txt
	@echo "1.2.3-0.30+skjflsakdf-09324023949fsda90fs" > example2.txt
	@echo "0.0.3-zeta-0.30+skjflsakdf-09324023949fsda90fs" >> example2.txt
